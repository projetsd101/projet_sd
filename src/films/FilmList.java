package films;
import java.util.ArrayList;


public class FilmList {

	
	private ArrayList<Film> filmlist;
	
	
	public FilmList (ArrayList<Film> filmlist) {
		this.filmlist = filmlist;
	}
	
	public FilmList (FilmList other) {
		this.filmlist=other.filmlist;
	}
	
	public FilmList () {
		this.filmlist = new ArrayList<Film>();
	}
	
	public ArrayList<Film> getFilmlist() {
		return filmlist;
	}

	public void setFilmlist(ArrayList<Film> filmlist) {
		this.filmlist = filmlist;
	}
	
	public void add(Film f) {
		this.filmlist.add(f);
	}
	
	public Film getFilm(int num) {
		return this.filmlist.get(num-1);
	}

	@Override
	public String toString() {
		String res = "";
		for (Film f : this.filmlist) {
			res += f.toString();
		}
		return res;
	}
	
	public String displayTitles() {
		String res = "";
		for (Film f : this.filmlist) {
			res += f.getTitle()+"\n";
		}
		return res;
	}
	
	public String displayShort() {
		String res = "";
		for (Film f : this.filmlist) {
			res += f.getNumber()+". "+f.getTitle()+"\n";
		}
		return res;
	}
	
	public String displayTitlesAndMatchmark() {
		String res = "";
		for (int i = 0; i<this.size(); i++) {
			Film f = this.getFilm(i+1);
			res += (i+1)+". "+f.getTitle()+"\n\tmatch mark : "+f.getMatchmark()+"\n";
		}
		return res;
	}
	
	public int size() {
		return this.filmlist.size();
	}
	
	public boolean equals(FilmList other) {
		return this.filmlist.equals(other.filmlist);
	}
	
}
