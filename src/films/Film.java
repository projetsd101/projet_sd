package films;
import java.util.ArrayList;

public class Film {

	private Integer number;
	private String title;
	private Integer date;
	private String type;
	private String description;
	private String director;
	private ArrayList<String> actors;
	private ArrayList<String> filmStyles;
	private Integer length;
	private Double mark;
	private boolean seen;
	private Integer matchmark;

	
	public Film(Integer number, String title, Integer date, String type,
			String description, String director, ArrayList<String> actors,
			ArrayList<String> filmStyles, Integer length) {
		this.number = number;
		this.title = title;
		this.date = date;
		this.type = type;
		this.description = description;
		this.director = director;
		this.actors = actors;
		this.filmStyles = filmStyles;
		this.length = length;
		mark = 2.5;
		seen = false;
		this.matchmark = 0;
	}

	public Film() {
		this.number = 0;
		this.title = "";
		this.date = 0;
		this.type = "";
		this.description = "";
		this.director = null;
		this.actors = null;
		this.filmStyles = null;
		this.length = 0;
		mark = (double) 0;
		seen = false;
		matchmark = 0;
	}

	public Integer getNumber() {
		return number;
	}

	
	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getMatchmark() {
		return matchmark;
	}

	public void setMatchmark(Integer matchmark) {
		this.matchmark = matchmark;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public ArrayList<String> getActors() {
		return actors;
	}

	public void setActors(ArrayList<String> actors) {
		this.actors = actors;
	}

	public ArrayList<String> getStyle() {
		return filmStyles;
	}

	public void setStyle(ArrayList<String> filmStyles) {
		this.filmStyles = filmStyles;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getMark() {
		return mark;
	}

	public void setMark(Double mark) {
		this.mark = mark;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	@Override
	public String toString() {
		String act = "";
		String st = "";
		String len = "";

		for (int i = 0; i < this.actors.size() - 1; i++) {
			act += this.actors.get(i) + ", ";
		}
		act += this.actors.get(this.actors.size() - 1);

		for (int j = 0; j < this.filmStyles.size() - 1; j++) {
			st += this.filmStyles.get(j) + " | ";
		}
		st += this.filmStyles.get(this.filmStyles.size() - 1);
		
		if (this.length != 0) len += this.length+" mins.";

		return this.number + ".  " + this.title + " (" + this.date + ")\n\n" + this.description
				+ "\n" + "Director : " + this.director + "\n" + "With : " + act
				+ "\n" + st + "  " + len + "\n\n\n";
	}

}
