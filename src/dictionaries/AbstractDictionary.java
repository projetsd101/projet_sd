package dictionaries;

import dictionaries.Dictionary;

public abstract class AbstractDictionary<V> implements  Dictionary<V> {

    /** La table est-elle vide ?
     */
    public boolean isEmpty () {
	return (size() == 0) ;
    } // isEmpty()
   
}
