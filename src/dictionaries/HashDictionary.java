package dictionaries;

import java.util.ArrayList;

import dictionaries.AbstractDictionary;
import dictionaries.Association;

public class HashDictionary<V> extends AbstractDictionary<V> {

	protected int size; // nombre d'elements dans la table de hachage
	protected ArrayList<ArrayList<Association<V>>> tab; // tableau pointant
															// sur les
															// sous-tables
	protected int nbSousTables; // nombre de sous-tables

	// Constructeurs

	/**
	 * Dictionnaire vide utilisant 10 sous-tables
	 */
	public HashDictionary() {
		this(10);
	}

	/**
	 * Dictionnaire vide utilisant n sous-tables
	 *
	 * @param n
	 *            nombre de sous-tables
	 */
	public HashDictionary(int n) {
		nbSousTables = n;
		tab = new ArrayList<ArrayList<Association<V>>>(n);
		for (int i = 0; i < n; i++)
			tab.add(new ArrayList<Association<V>>());
	}

	/**
	 * Calcul du hashCode d'une cle
	 *
	 * @param key
	 *            cle
	 * @return hashCode compris entre 0 et n
	 */
	public int hashCode(String key) {
		return Math.abs((new KeyType(key)).hashCode()) % nbSousTables;
	}

	/**
	 * Ajouter un couple < cle, valeur >
	 * 
	 * @param key
	 *            cle
	 * @param value
	 *            valeur
	 */
	public void add(String key, V value) {
		Association<V> a = new Association<V>(new KeyType(key), value);
		int h = this.hashCode(key);
		tab.get(h).add(a);
		size++;
	}

	/**
	 * Supprimer une cle
	 * 
	 * @param key
	 *            cle
	 */
	public void delete(String key) {
		int h = this.hashCode(key);
		int i = 0;
		while (i < tab.get(h).size() && tab.get(h).get(i).getKey().getK() != key)
			i++;
		tab.get(h).remove(i);
		size--;
	}
	
	/**
	 * Modifier la valeur associ�e � une cl�
	 * 
	 * @param key
	 * 			cle
	 */
	public void set(String key, V val) {
		int h = this.hashCode(key);
		int i = 0;
		while (i < tab.get(h).size() && ! tab.get(h).get(i).getKey().getK().equals(key))
			i++;
		tab.get(h).get(i).setValue(val);
	}

	/**
	 * Tester l'existence d'une cle
	 */
	public boolean has(String key) {
		int h = this.hashCode(key);
		for (Association<V> a : tab.get(h)) {
			if (a.getKey().getK().equals(key))
				return true;
		}
		return false;
	}

	/**
	 * Consulter la valeur attachee a une cle
	 *
	 * @param key
	 *            cle recherchee
	 * @return null si la cle n'existe pas, la valeur sinon
	 */
	public V value(String key) {
		int h = this.hashCode(key);
		int c = 1;
		for (Association<V> a : tab.get(h)) {
			if (a.getKey().getK().equals(key)) {
				return a.getValue();
			}
			c++;
		}
		return null;
	}

	/**
	 * Nombre d'entrees dans la table
	 */
	public int size() {
		return size;
	}

	/**
	 * Affichage de la table (i.e. les elements de chaque sous-table)
	 */
	public void afficher() {
		for (int i = 0; i < tab.size(); i++) {
			System.out.println("Sous-table h=" + i);
			ArrayList<Association<V>> sousTable = (ArrayList<Association<V>>) tab
					.get(i);
			for (int j = 0; j < sousTable.size(); j++) {
				Association<V> asso = sousTable.get(j);
				System.out.println("    " + asso.toString());
			}
		}
	}

}
