package dictionaries;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class HashDictionaryTest {

	@Test
	public void testHashDictionary() {
		HashDictionary<Double> d = new HashDictionary<Double>();
		assertEquals(10, d.nbSousTables);
		assertEquals(0, d.size);
	}

	@Test
	public void testHashDictionaryInt() {
		int n = 3;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		assertEquals(n, d.nbSousTables);
		assertEquals(0, d.size);
	}

	@Test
	public void testHashCodeString() {
		String key1 = "Bonjour";
		String key2 = "Jambon";
		String key3 = "J'aime la SD";
		int n = 5;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		int h1 = (new KeyType(key1)).hashCode() % n;
		int h2 = (new KeyType(key2)).hashCode() % n;
		int h3 = (new KeyType(key3)).hashCode() % n;
		
		assertEquals(h1, d.hashCode(key1));
		assertEquals(h2, d.hashCode(key2));
		assertEquals(h3, d.hashCode(key3));
	}

	@Test
	public void testAdd() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key = "Bonjour";
		Double val = 6.56;
		d.add(key, val);
		int h = d.hashCode(key);
		Association<Double> a = new Association<Double>(new KeyType(key), val);
		
		assertTrue(d.has(key));
		assertEquals(val, d.value(key));
		assertTrue(d.tab.get(h).get(0).equals(a));
	}

	@Test
	public void testDelete() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key = "Bonjour";
		Double val = 6.56;
		d.add(key, val);
		d.delete(key);
		
		assertFalse(d.has(key));
	}

	@Test
	public void testSet() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key = "Bonjour";
		Double val = 6.56;
		d.add(key, val);
		Double val2 = 4.43;
		d.set(key, val2);
		
		assertEquals(val2, d.value(key));
	}

	@Test
	public void testHas() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key1 = "Bonjour";
		String key2 = "Au revoir";
		Double val = 6.56;
		d.add(key1, val);
		
		assertTrue(d.has(key1));
		assertFalse(d.has(key2));
		
		d.delete(key1);
		
		assertFalse(d.has(key1));
	}

	@Test
	public void testValue() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key = "Bonjour";
		Double val = 6.56;
		d.add(key, val);
		
		assertEquals(val, d.value(key));
	}

	@Test
	public void testSize() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key1 = "Bonjour";
		String key2 = "Jambon";
		Double val1 = 6.56;
		Double val2 = 4.43;
		
		assertEquals(0, d.size());
		
		d.add(key1, val1);
		d.add(key2, val2);
		assertEquals(2, d.size());
		
		d.delete(key1);
		assertEquals(1, d.size());
		
	}

	@Test
	public void testIsEmpty() {
		int n = 4;
		HashDictionary<Double> d = new HashDictionary<Double>(n);
		String key = "Bonjour";
		Double val = 6.56;
		
		assertTrue(d.isEmpty());
		
		d.add(key, val);
		assertFalse(d.isEmpty());
		
		d.delete(key);
		assertTrue(d.isEmpty());
		
	}

}
