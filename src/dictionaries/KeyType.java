package dictionaries;

public class KeyType {

    private String k;
	
    public KeyType(String k){
       this.k = k;
    }
	
    public String getK(){
      return k;
    }
		
    public boolean equals(Object o){
      return(this.k.equals( ((KeyType)o).getK()) );
    }

    public String toString(){
      return k;
    }

    public int hashCode(){
	  return this.k.length();
    }

}
