package dictionaries;

public interface Dictionary<V> {

    /** Ajouter un couple < cle, valeur > >
     */
    public abstract void add (String key, V value) ;

    /** Tester l'existence d'une cl�
     */
    public boolean has (String key) ;

    /** Consulter la valeur attachee a une cle
     */
    public abstract V value (String key) ;

    /** Supprimer une cle
     */
    public abstract void delete (String key) ;

    /** La table est-elle vide ?
     */
    public abstract boolean isEmpty () ;
   
    /** Nombre d'entrees dans la table
     */
    public abstract int size() ;
         
}
