package dictionaries;

import static org.junit.Assert.*;

import org.junit.Test;

public class AssociationTest {

	@Test
	public void testAssociation() {
		KeyType s = new KeyType("toto");
		Double n=0.;
		Association<Double> test = new Association<Double> (s ,n);
		assertEquals(s,test.key);
		assertEquals(n, test.value);
	}

	@Test
	public void testGetKey() {
		KeyType s = new KeyType ("test trop coool");
		Association<Double> test = new Association<Double> (s,0.);
		assertEquals(s,test.getKey());
	}

	@Test
	public void testGetValue() {
		KeyType s = new KeyType ("test trop coool");
		Double v = 0.;
		Association<Double> test = new Association<Double> (s,v);
		assertEquals(v,test.getValue());
	}
	

	@Test
	public void testSetValue() {
		KeyType s = new KeyType ("test trop coool");
		Double v = 0.;
		Double h=2.;
		Association<Double> test = new Association<Double> (s,h);
		test.setValue(v);
		assertEquals(v,test.getValue());
	}
	
	@Test
	public void testEqualsAssociation() {
		KeyType s = new KeyType ("test trop coool");
		Double v = 0.;
		Association<Double> test = new Association<Double> (s,v);
		Association<Double> testcopy = new Association<Double> (s,v);
		assertTrue(test.equals(testcopy));
	}

}
