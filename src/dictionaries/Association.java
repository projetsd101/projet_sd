package dictionaries;

public class Association<V> {

    protected KeyType key ;  // cle
    protected V value ; // valeur 

    /** 
	@param k la cle
	@param v la valeur
     */
    public Association(KeyType k, V v) {
	key = k ;
	value = v ;
    } 
   
    /** Acces a la cle
     */
    public KeyType getKey () {
	return key ;
    } 

    /** Acces a la valeur
     */
    public V getValue () {
	return value ;
    } 
    
    /** Modification de la valeur
     */
    public void setValue(V val) {
    	this.value = val;
    }

    /** Version String < cl�, valeur >
     */
    public String toString () {
	return "<"+key+","+value+">" ;
    } 
    
    public boolean equals(Association<V> other) {
    	return (this.key).equals(other.key) && (this.value).equals(other.value);
    }
 
} 

