package dictionaries;

import static org.junit.Assert.*;

import org.junit.Test;

public class KeyTypeTest {

	@Test
	public void testHashCode() {
		String s = "Joffrey DA ROCHA";
		KeyType test = new KeyType(s);
		assertEquals(s.length(),test.hashCode());
	}

	@Test
	public void testKeyType() {
		String s = "Joffrey DA ROCHA";
		KeyType test = new KeyType(s);
		assertEquals(s,test.getK()); 
	}

	@Test
	public void testGetK() {
		String s = "Joffrey DA ROCHA";
		KeyType test = new KeyType(s);
		assertEquals(s,test.getK()); 
	}

	@Test
	public void testEqualsObject() {
		String s = "Joffrey DA ROCHA";
		KeyType test = new KeyType(s);
		KeyType test2 = new KeyType(s);
		assertTrue(test.equals(test2));
	}


}
