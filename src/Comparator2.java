import java.util.ArrayList;

import dictionaries.*;
import films.*;


public class Comparator2 {
	

	
	private HashDictionary<Double> dir_count;
	private HashDictionary<Double> act_count;
	private HashDictionary<Double> styles_count;
	private ArrayList<Film> list;
	
	
	public Comparator2(HashDictionary<Double> dir_count,
			HashDictionary<Double> act_count,
			HashDictionary<Double> styles_count, ArrayList<Film> list) {
	
		this.dir_count = dir_count;
		this.act_count = act_count;
		this.styles_count = styles_count;
		this.list = list;
	}
	
	public HashDictionary<Double> getDir_count() {
		return dir_count;
	}
	public void setDir_count(HashDictionary<Double> dir_count) {
		this.dir_count = dir_count;
	}
	public HashDictionary<Double> getAct_count() {
		return act_count;
	}
	public void setAct_count(HashDictionary<Double> act_count) {
		this.act_count = act_count;
	}
	public HashDictionary<Double> getStyles_count() {
		return styles_count;
	}
	public void setStyles_count(HashDictionary<Double> styles_count) {
		this.styles_count = styles_count;
	}
	public ArrayList<Film> getList() {
		return list;
	}
	public void setList(ArrayList<Film> list) {
		this.list = list;
	}
	
	
	 public void sort(ArrayList<Film> film) {
		Film tmp;
		int j;
		for (int i = 1; i < film.size(); i++) {
			tmp = film.get(i);
			j = i;
			while (j>0 && film.get(j-1).getMatchmark()<tmp.getMatchmark()) {
				film.set(j, film.get(j-1));
				j--;
			}
			film.set(j, tmp);
		}
		this.setList(film);
	}
	 
	
	public void compare () {
		
		for (Film f : this.list) {
			int style_mm = 0;
			int actor_mm=0;
			int dir_mm=0;
			
			for (String s : f.getStyle()) {
				if (this.styles_count.has(s))
					style_mm += this.styles_count.value(s);
			}
			for (String a : f.getActors()) {
				if (this.act_count.has(a))
					actor_mm += this.act_count.value(a);
			}
			if (this.dir_count.has(f.getDirector()))
				dir_mm += this.dir_count.value(f.getDirector());
			f.setMatchmark(3*style_mm + 3*actor_mm + 10*dir_mm);
		}
		this.sort(this.list);
	}
	
}
