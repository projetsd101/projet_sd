import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import dictionaries.*;
import films.*;

public class Main {

	static boolean nothingSeen = true;

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		if (args.length == 0) {
			System.out.println("Nombre d'arguments incorrect");
			System.out.println("\t--> java -ea Main sourceFile");
			return;
		}
		
		FilmList fl = createList(args[0]);
		System.out.println("Quel mode de s�lection d�sirez-vous ?");
		System.out.println("Film par film (1) ou afficher la liste et choisir dedans (2) ?");
		FilmList ref;
		
		int r = s.nextInt();
		while (r != 1 && r !=2) {
			r = s.nextInt();
		}
		if (r==1)
			ref = chooseFilms(fl);
		else
			ref = chooseFilms2(fl);
		
		markFilms(ref);
		Comparator2 comp = analyse(ref);

		FilmList unseen = new FilmList();
		for (Film f : fl.getFilmlist()) {
			if (!f.isSeen()) {
				unseen.add(f);
			}
		}
		comp.setList(unseen.getFilmlist());

		comp.compare();

		FilmList l = new FilmList(comp.getList());
		while (l.size() > 5) {
			l.getFilmlist().remove(5);
		}

		showDetails(l);

		System.out
				.println("\n\nMerci d'avoir utilis� notre programme de recommandation !");

	}

	public static String noSpaces(String str) {
		String res = str;
		while (res.startsWith(" ")) {
			res = res.substring(1, res.length());
		}
		while (res.endsWith(" ")) {
			res = res.substring(0, res.length() - 1);
		}
		return res;
	}

	public static FilmList createList(String src) {
		ArrayList<Film> filmsList = new ArrayList<Film>();
		FilmList fl;

		// LECTURE ET INITIALISATION

		FileReader fr;
		BufferedReader br;

		try {
			fr = new FileReader(src);
			br = new BufferedReader(fr);

			String line;

			Integer number;
			String title;
			Integer date;
			String type;
			String description;
			String director;
			Integer length = 0;

			do {
				ArrayList<String> style = new ArrayList<String>();
				ArrayList<String> actors = new ArrayList<String>();
				String[] listTab;

				line = br.readLine();
				// line = 1. Fast &amp; Furious 7 (2015)
				listTab = line.split("\\.", 2);
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				number = Integer.parseInt(listTab[0]);
				line = listTab[1].substring(2);
				// line = Fast &amp; Furious 7 (2015)
				listTab = line.split("\\(");
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				title = noSpaces(listTab[0].substring(0,
						listTab[0].length() - 1));
				line = listTab[1];
				// line = 2015)
				if (line.length() <= 7) {
					date = Integer
							.parseInt(line.substring(0, line.length() - 1));
					type = "Film";
				} else {
					listTab = line.split(" ", 2);
					// System.out.println(listTab.length);
					// System.out.println(Arrays.toString(listTab));
					date = Integer.parseInt(listTab[0]);
					type = "TV Series";
				}

				line = br.readLine();
				// line =

				line = br.readLine();
				// line = Deckard Shaw seeks revenge against Dominic Toretto and
				// his family for his comatose brother.
				description = line;

				line = br.readLine();
				if (line.substring(0, 8).equals("Director")) {
					// line = Director : James Wan
					listTab = line.split(":", 2);
					// System.out.println(listTab.length);
					// System.out.println(Arrays.toString(listTab));
					director = noSpaces(listTab[1].substring(1));
					line = br.readLine();
				} else {
					director = "Unknown";
				}
				// line = With : Vin Diesel, Paul Walker, Dwayne Johnson
				listTab = line.split(":", 2);
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				listTab = listTab[1].split(",");
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				for (int i = 0; i <= listTab.length - 1; i++) {
					actors.add(noSpaces(listTab[i]));
				}

				line = br.readLine();
				// line = Action | Crime | Thriller 137 mins.
				listTab = line.split("\\|");
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				for (int i = 0; i <= listTab.length - 2; i++) {
					style.add(noSpaces(listTab[i]));
				}
				line = listTab[listTab.length - 1];
				// line = Thriller 137 mins.
				if (line.startsWith(" "))
					listTab = line.substring(1).split("\\ ", 3);
				else
					listTab = line.split("\\ ", 3);
				// System.out.println(listTab.length);
				// System.out.println(Arrays.toString(listTab));
				style.add(noSpaces(listTab[0]));
				if (listTab.length > 1)
					length = Integer.parseInt(listTab[1]);
				else
					length = 0;

				line = br.readLine();
				line = br.readLine();

				filmsList.add(new Film(number, title, date, type, description,
						director, actors, style, length));

			} while (line != null);

			br.close();
		} catch (IOException e) {
		}

		fl = new FilmList(filmsList);

		// System.out.println(fl);
		System.out.println("\nListe de films import�e.");
		return fl;

	}

	public static FilmList chooseFilms(FilmList fl)
			throws NumberFormatException {
		System.out.println("\nQuels films parmi cette liste avez-vous vu ?");
		System.out
				.println("\nPour chaque film, veuillez entrer :\n\t'o' si vous l'avez vu,\n\trien si vous ne l'avez pas vu.");
		System.out.println("Si vous n'avez vu aucun film, entrez 'r'\n");

		Scanner s = new Scanner(System.in);
		String r;

		ArrayList<Film> res = new ArrayList<Film>();

		for (int i = 1; i < fl.size(); i++) {
			Film f = fl.getFilm(i);
			System.out.print(f.getTitle() + " : ");
			r = s.nextLine();
			if (r.equals("r")) {
				nothingSeen = true;
				return new FilmList();
			}
			if (!r.equals("") && r.substring(0, 1).equals("o")) {
				System.out.println("Vu");
				nothingSeen = false;
				f.setSeen(true);
				res.add(f);
			} else
				System.out.println("Non");
		}
		return new FilmList(res);
		
	}

	public static FilmList chooseFilms2(FilmList fl) {
		System.out.println("La vid�oth�que contient les films suivants :");
		System.out.println(fl.displayShort());
		System.out.println("Quels films parmi ceux-ci avez-vous vus ? Entrez leurs num�ros s�par�s par des espaces, ou 'r' si vous n'avez rien vu");

		Scanner s = new Scanner(System.in);
		String r = s.nextLine();

		if (r.equals("r"))
			return new FilmList();

		ArrayList<Film> res = new ArrayList<Film>();
		nothingSeen = false;

		String[] rtab = r.split(" ");
		int ignored = 0;
		Film f;

		for (int i = 0; i < rtab.length; i++) {
			try {
				if (Integer.parseInt(rtab[i]) > fl.size() || Integer.parseInt(rtab[i]) <= 0)
					ignored++;
				else {
					f = fl.getFilmlist().get(Integer.parseInt(rtab[i]) - 1);
					f.setSeen(true);
					res.add(f);
				}
			} catch (NumberFormatException e) {
				ignored++;
			}
		}

		if (ignored != 0)
			System.out.println("ignor�s : " + ignored+"\n");
		return new FilmList(res);
	}
	
	public static void markFilms(FilmList ref) {
		if (!nothingSeen) {

			System.out.println("\n\nVous avez vu les films suivants :");
			System.out.println(ref.displayTitles());

			System.out
					.println("\nPouvez-vous les noter sur 5 ? ('n' pour ne pas noter)");
			Scanner s = new Scanner(System.in);
			String r;
			for (Film f : ref.getFilmlist()) {
				System.out.print(f.getTitle() + " : ");
				do {
					r = s.nextLine();
				} while (r.equals(""));
				if (!r.equals("n")) {
					try {
						Double d = Double.parseDouble(r);
						while (d < 0 || d > 5) {
							System.out
									.println("La note doit �tre comprise entre 0 et 5");
							r = s.nextLine();
							d = Double.parseDouble(r);
						}
						f.setMark(Double.parseDouble(r));
					} catch (NumberFormatException e) {
						System.out.println("Avis non pris en compte");
					}
				}
			}

			System.out.println("\nAvis enregistr�s.");
		} else {
			System.out.println("\nVous n'avez vu aucun film");
		}
	}
	
	public static Comparator2 analyse(FilmList ref) {
		HashDictionary<Double> dir_count = new HashDictionary<Double>(4);
		HashDictionary<Double> act_count = new HashDictionary<Double>(4);
		HashDictionary<Double> styles_count = new HashDictionary<Double>(4);

		if (!ref.equals(new FilmList())) {
			Double m;
			String tmp;

			for (Film f : ref.getFilmlist()) {
				m = f.getMark() - 1.5;

				// DIRECTOR :
				tmp = f.getDirector();
				if (dir_count.has(tmp))
					dir_count.set(tmp, dir_count.value(tmp) * m);
				else
					dir_count.add(tmp, m);

				// ACTORS :
				for (String a : f.getActors()) {
					tmp = a;
					if (act_count.has(tmp))
						act_count.set(tmp, act_count.value(tmp) * m);
					else
						act_count.add(tmp, m);
				}

				// STYLES :
				for (String s : f.getStyle()) {
					tmp = s;
					if (styles_count.has(tmp))
						styles_count.set(tmp, styles_count.value(tmp) * m);
					else
						styles_count.add(tmp, m);
				}

			}

		} else {
			System.out
					.println("\nY a-t-il des genres que vous aimez particuli�rement ?");
			System.out
					.println("Entrez les num�ros des genres, s�par�s par des espaces :");

			String[] stylesList = { "Action", "Crime", "Thriller", "Drama",
					"Horror", "Adventure", "Family", "Fantasy", "Romance",
					"Sci-Fi", "Animation", "Comedy", "Mystery", "History",
					"Musical", "Sport", "Biography", "War" };

			for (int i = 0; i < stylesList.length; i++) {
				System.out.println((i + 1) + ". " + stylesList[i]);
			}

			Scanner s = new Scanner(System.in);
			String r = s.nextLine();

			String[] rtab = r.split(" ");
			int ignored = 0;

			for (int i = 0; i < rtab.length; i++) {
				try {
					if (Integer.parseInt(rtab[i]) > stylesList.length
							|| Integer.parseInt(rtab[i]) <= 0)
						ignored++;
					else {
						styles_count.add(
								stylesList[Integer.parseInt(rtab[i]) - 1], 1.);
					}
				} catch (NumberFormatException e) {
				}
			}

			System.out.println("\nPr�f�rences enregistr�es.");

			if (ignored != 0)
				System.out.println("ignor�s : " + ignored);

		}

		return new Comparator2(dir_count, act_count, styles_count,
				new ArrayList<Film>());
	}

	public static void showDetails(FilmList l) {

		System.out.println("Nous vous proposons les films suivants :");
		System.out.println(l.displayTitlesAndMatchmark());
		System.out
				.println("\nVoulez-vous les d�tails de certains films ? (num�ros, 't' pour tous ou 'n' pour non)");

		Scanner s = new Scanner(System.in);
		String r = s.nextLine();

		if (r.equals("n"))
			return;

		ArrayList<Film> res = new ArrayList<Film>();
		if (r.equals("t")) {
			res = l.getFilmlist();
			System.out.println((new FilmList(res)).toString());
			return;
		}

		String[] rtab = r.split(" ");
		int ignored = 0;

		for (int i = 0; i < rtab.length; i++) {
			try {
				if (Integer.parseInt(rtab[i]) > l.size()
						|| Integer.parseInt(rtab[i]) <= 0)
					ignored++;
				else {
					res.add(l.getFilmlist().get(Integer.parseInt(rtab[i]) - 1));
				}
			} catch (NumberFormatException e) {
				ignored++;
			}
		}

		System.out.println("\nVous avez choisi les films suivants :");
		System.out.println((new FilmList(res)).toString());
		if (ignored != 0)
			System.out.println("ignor�s : " + ignored);

	}

}
